package ru.tsc.apozdnov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.apozdnov.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.apozdnov.tm.api.service.dto.IServiceDTO;
import ru.tsc.apozdnov.tm.dto.model.AbstractModelDto;
import ru.tsc.apozdnov.tm.exception.field.EmptyIdException;

import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractServiceDTO<M extends AbstractModelDto, R extends IRepositoryDTO<M>> implements IServiceDTO<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected abstract IRepositoryDTO<M> getRepository();

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        repository.add(model);
    }

    @Override
    @Transactional
    public void set(@NotNull final Collection<M> models) {
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        repository.set(models);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        repository.update(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        repository.remove(model);
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        repository.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.findOneById(id);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        repository.removeById(id);
    }

    @Override
    public long getCount() {
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.getCount();
    }

}