package ru.tsc.apozdnov.tm.api.repository.dto;

import ru.tsc.apozdnov.tm.dto.model.ProjectDto;

public interface IProjectRepositoryDTO extends IUserOwnedRepositoryDTO<ProjectDto> {

}
