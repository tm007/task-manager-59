package ru.tsc.apozdnov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.apozdnov.tm.api.repository.dto.IUserRepositoryDTO;
import ru.tsc.apozdnov.tm.api.service.IPropertyService;
import ru.tsc.apozdnov.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.apozdnov.tm.dto.model.UserDto;
import ru.tsc.apozdnov.tm.enumerated.Role;
import ru.tsc.apozdnov.tm.exception.entity.UserNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.*;
import ru.tsc.apozdnov.tm.util.HashUtil;

import java.util.Optional;

@Service
public class UserServiceDTO extends AbstractServiceDTO<UserDto, IUserRepositoryDTO> implements IUserServiceDTO {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserRepositoryDTO repository;

    @NotNull
    @Override
    protected IUserRepositoryDTO getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto create(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final IUserRepositoryDTO repository = getRepository();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @NotNull final IUserRepositoryDTO repository = getRepository();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto create(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final Role role
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final IUserRepositoryDTO repository = getRepository();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        if (role == null) user.setRole(Role.USUAL);
        else user.setRole(role);
        repository.add(user);
        return user;
    }

    @Nullable
    @Override
    public UserDto findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserRepositoryDTO repository = getRepository();
        return repository.findOneByLogin(login);
    }

    @Nullable
    @Override
    public UserDto findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserRepositoryDTO repository = getRepository();
        return repository.findOneByEmail(email);
    }

    @Override
    @Transactional
    public void removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        remove(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDto setPassword(@NotNull final String id, @NotNull final String password) {
        if (id.isEmpty()) throw new EmptyIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserDto user = findOneById(id);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(password, secret, iteration));
        update(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDto updateUser(
            @NotNull final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final UserDto user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    @Transactional
    public void lockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        final UserDto user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
        update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        final UserDto user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
        update(user);
    }

}