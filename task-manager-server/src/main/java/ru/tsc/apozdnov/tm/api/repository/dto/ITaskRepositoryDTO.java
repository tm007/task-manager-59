package ru.tsc.apozdnov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.model.TaskDto;

import java.util.List;

public interface ITaskRepositoryDTO extends IUserOwnedRepositoryDTO<TaskDto> {

    @NotNull
    List<TaskDto> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(String projectId);

}