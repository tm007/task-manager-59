package ru.tsc.apozdnov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.UserDto;

public interface IUserRepositoryDTO extends IRepositoryDTO<UserDto> {

    @Nullable
    UserDto findOneByLogin(@NotNull String login);

    @Nullable
    UserDto findOneByEmail(@NotNull String email);

}