package ru.tsc.apozdnov.tm.api.service.dto;

import ru.tsc.apozdnov.tm.dto.model.ProjectDto;

public interface IProjectServiceDTO extends IUserOwnedServiceDTO<ProjectDto> {

}