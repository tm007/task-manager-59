package ru.tsc.apozdnov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.apozdnov.tm.dto.model.ProjectDto;
import ru.tsc.apozdnov.tm.enumerated.Sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Repository
public class ProjectRepositoryDTO extends AbstractUserOwnedRepositoryDTO<ProjectDto> implements IProjectRepositoryDTO {

    @NotNull
    @Override
    public List<ProjectDto> findAll() {
        @NotNull final String jpql = "SELECT m FROM ProjectDto m";
        return entityManager.createQuery(jpql, ProjectDto.class).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@NotNull String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM ProjectDto m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@NotNull String userId, @NotNull Sort sort) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM ProjectDto m WHERE m.userId = :userId ORDER BY m."
                + getSortType(sort.getComparator());
        return entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDto> findAll(@NotNull String userId, @NotNull Comparator<ProjectDto> comparator) {
        if (userId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM ProjectDto m WHERE m.userId = :userId ORDER BY m." + getSortType(comparator);
        return entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDto findOneById(@NotNull String id) {
        return entityManager.find(ProjectDto.class, id);
    }

    @Nullable
    @Override
    public ProjectDto findOneById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String jpql = "SELECT m FROM ProjectDto m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String id) {
        Optional<ProjectDto> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    public void removeById(@NotNull String userId, @NotNull String id) {
        Optional<ProjectDto> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM ProjectDto";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void clear(@NotNull String userId) {
        if (userId.isEmpty()) return;
        @NotNull final String jpql = "DELETE FROM ProjectDto m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public long getCount() {
        @NotNull final String jpql = "SELECT COUNT(e) FROM ProjectDto e";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getCount(@NotNull String userId) {
        @NotNull final String jpql = "SELECT COUNT(e) FROM ProjectDto e WHERE e.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM ProjectDto m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        if (userId.isEmpty() || id.isEmpty()) return false;
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM ProjectDto m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult();
    }

}