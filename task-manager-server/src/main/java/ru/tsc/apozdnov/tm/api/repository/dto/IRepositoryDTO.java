package ru.tsc.apozdnov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.AbstractModelDto;

import java.util.Collection;
import java.util.List;

public interface IRepositoryDTO<M extends AbstractModelDto> {

    void add(@NotNull M model);

    void set(@NotNull Collection<M> models);

    void update(@NotNull M model);

    void remove(@NotNull M model);

    void removeById(@NotNull String id);

    void clear();

    long getCount();

    boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@NotNull String id);

    @NotNull
    List<M> findAll();

}