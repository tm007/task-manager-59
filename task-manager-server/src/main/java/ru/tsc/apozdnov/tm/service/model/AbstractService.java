package ru.tsc.apozdnov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.apozdnov.tm.api.repository.model.IRepository;
import ru.tsc.apozdnov.tm.api.service.model.IService;
import ru.tsc.apozdnov.tm.exception.field.EmptyIdException;
import ru.tsc.apozdnov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected abstract IRepository<M> getRepository();

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        @NotNull final IRepository<M> repository = getRepository();
        repository.add(model);
    }

    @Override
    @Transactional
    public void set(@NotNull final Collection<M> models) {
        @NotNull final IRepository<M> repository = getRepository();
        repository.set(models);
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final IRepository<M> repository = getRepository();
        repository.update(model);
    }

    @Override
    @Transactional
    public void remove(@NotNull final M model) {
        @NotNull final IRepository<M> repository = getRepository();
        repository.remove(model);
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final IRepository<M> repository = getRepository();
        repository.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final IRepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final IRepository<M> repository = getRepository();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IRepository<M> repository = getRepository();
        return repository.findOneById(id);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IRepository<M> repository = getRepository();
        repository.removeById(id);
    }

    @Override
    public long getCount() {
        @NotNull final IRepository<M> repository = getRepository();
        return repository.getCount();
    }

}