package ru.tsc.apozdnov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.apozdnov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.apozdnov.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.apozdnov.tm.dto.model.ProjectDto;
import ru.tsc.apozdnov.tm.exception.field.EmptyDescriptionException;
import ru.tsc.apozdnov.tm.exception.field.EmptyNameException;
import ru.tsc.apozdnov.tm.exception.field.EmptyUserIdException;

import java.util.Date;

@Service
public class ProjectServiceDTO extends AbstractUserOwnedServiceDTO<ProjectDto, IProjectRepositoryDTO>
        implements IProjectServiceDTO {

    @NotNull
    @Autowired
    private IProjectRepositoryDTO repository;

    @NotNull
    @Override
    protected IProjectRepositoryDTO getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDto create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable ProjectDto project = new ProjectDto();
        project.setName(name);
        project.setUserId(userId);
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        repository.add(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable ProjectDto project = new ProjectDto();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        repository.add(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable ProjectDto project = new ProjectDto();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        @NotNull final IProjectRepositoryDTO repository = getRepository();
        repository.add(project);
        return project;
    }

}