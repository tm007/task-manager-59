package ru.tsc.apozdnov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.tsc.apozdnov.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.apozdnov.tm.comparator.CreatedComparator;
import ru.tsc.apozdnov.tm.comparator.DateBeginComparator;
import ru.tsc.apozdnov.tm.comparator.StatusComparator;
import ru.tsc.apozdnov.tm.dto.model.AbstractUserOwnedModelDto;

import java.util.Comparator;

@Repository
public abstract class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDto> extends AbstractRepositoryDTO<M>
        implements IUserOwnedRepositoryDTO<M> {

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateBeginComparator.INSTANCE) return "status";
        else return "name";
    }

    public void add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.persist(model);
    }

    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.remove(model);
    }

}