package ru.tsc.apozdnov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.model.TaskDto;

import java.util.List;

public interface ITaskServiceDTO extends IUserOwnedServiceDTO<TaskDto> {

    @NotNull
    List<TaskDto> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}