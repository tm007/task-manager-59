package ru.tsc.apozdnov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.LogDto;

public interface ILoggerService {

    void writeLog(@NotNull LogDto message);

}