package ru.tsc.apozdnov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.TaskDto;

import java.util.List;

@Getter
@NoArgsConstructor
public final class TaskListByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<TaskDto> tasks;

    public TaskListByProjectIdResponse(@Nullable final List<TaskDto> tasks) {
        this.tasks = tasks;
    }

}