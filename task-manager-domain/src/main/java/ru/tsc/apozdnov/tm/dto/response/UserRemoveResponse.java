package ru.tsc.apozdnov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.dto.model.UserDto;

@NoArgsConstructor
public final class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@Nullable final UserDto user) {
        super(user);
    }

}